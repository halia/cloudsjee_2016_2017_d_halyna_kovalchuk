package pl.kovalchuk;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.DistributedFileSystem;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.filecache.DistributedCache;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Random;

public class JobTool extends Configured implements Tool {

    private final File inputFile;
    private final long id;

    public JobTool(File inputFile, long id) {
        this.inputFile = inputFile;
        this.id =id;
    }

    public int run(String[] strings) throws Exception {
        Configuration configuration = createConfig();
        Job job = configureJob(configuration);
        FileInputFormat.addInputPath(job, new Path("/user/vagrant/input"));
        FileOutputFormat.setOutputPath(job, new Path("/user/vagrant/output/"+id));
        job.submit();
        return 0;
    }

    private Job configureJob(Configuration configuration) throws IOException {
        Job job = Job.getInstance(configuration, "");
        job.setJarByClass(JobTool.class);
        job.setMapperClass(PasswordEntropyMapper.class);
        job.setCombinerClass(PasswordEntropyReducer.class);
        job.setReducerClass(PasswordEntropyReducer.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(DoubleWritable.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(DoubleWritable.class);
        return job;
    }

    protected Configuration createConfig() throws IOException {
        Configuration configuration = this.getConf();
        prepareValues(configuration);
        addJobToVM(configuration);
        return configuration;
    }

    private void prepareValues(Configuration configuration) {
        configuration.set("fs.hdfs.impl", DistributedFileSystem.class.getName());
        configuration.set("fs.file.impl", LocalFileSystem.class.getName());
        configuration.set("mapreduce.jobtracker.address", "192.168.5.10:54311");
        configuration.set("mapreduce.framework.name", "yarn");
        configuration.set("dfs.replication", "2");
        configuration.set("fs.defaultFS", "hdfs://192.168.5.10:9000");
        configuration.set("yarn.resourcemanager.hostname", "192.168.5.10");
        configuration.set("yarn.nodemanager.aux-services", "mapreduce_shuffle");
        configuration.set("mapreduce.app-submission.cross-platform", "true");
    }

    private void addJobToVM(Configuration configuration) throws IOException {
        addJarToDistributedCache(PasswordEntropyMapper.class, configuration);
        addJarToDistributedCache(REntropy.class, configuration);
        addFileToDistributedCache(inputFile, configuration);
    }

    private void addJarToDistributedCache(
            Class clazz, Configuration configuration)
            throws IOException {
        String path = retrieveJarPath(clazz);
        path = URLDecoder.decode(path);
        File file = new File(path);

        int number = new Random().nextInt();
        Path hdfsJar = new Path("/user/vagrant/lib" + number + "/"  + file.getName());

        FileSystem hdfs = FileSystem.get(configuration);

        hdfs.copyFromLocalFile(false, true, new Path(path), hdfsJar);
        DistributedCache.addFileToClassPath(hdfsJar, configuration);
    }

    private String retrieveJarPath(Class classToAdd) {
        return classToAdd.getProtectionDomain().
                getCodeSource().getLocation().
                getPath();
    }

    private void addFileToDistributedCache(File file, Configuration conf) throws IOException {
        Path hdfsJar = new Path("/user/vagrant/input/" + file.getName());
        FileSystem hdfs = FileSystem.get(conf);
        boolean deleteSourceFiles = false;
        boolean overwrite = true;
        hdfs.copyFromLocalFile(deleteSourceFiles, overwrite, new Path(file.getPath()), hdfsJar);
        DistributedCache.addFileToClassPath(hdfsJar, conf);
    }

    public void setCinfiguration(){
        this.setConf(new Configuration());
    }

}
