package pl.kovalchuk;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.IOUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@RestController
public class WebController {

    private long id;
   private File file= new File("D:\\Lodz 3 semestr\\Nowak JAVA\\Hadoop\\hadoop\\vagrant\\password.txt");
    		@GetMapping("/create")
    public String test() {
        try {
           id = System.nanoTime();
            new JobRunner().run(file, id);
            return "Job is working";
        } catch (Exception e) {
            e.printStackTrace();
            return "Job doesn't work";
        }
    }
@RequestMapping(value = "/getResult", produces = "text/plain")
    public String getResult() throws IOException {
        JobTool jobTool = new JobTool(file, id);
        jobTool.setCinfiguration();
        Configuration configuration = jobTool.createConfig();
        Path hdfsJar = new Path("/user/vagrant/output/" + id);
        FileSystem hdfs = FileSystem.get(configuration);
        String result = "";
        RemoteIterator<LocatedFileStatus> files = hdfs.listFiles(hdfsJar, false);
        while (files.hasNext()) {
            LocatedFileStatus nextFile = files.next();
            Path nextPath = nextFile.getPath();

            byte[] nextBuffer = new byte[(int) nextFile.getLen()];
            FSDataInputStream nextIn = hdfs.open(nextPath);
            IOUtils.readFully(nextIn, nextBuffer, 0, nextBuffer.length);
            String nextString = new String(nextBuffer, StandardCharsets.UTF_8);
            result += nextString;
        }
        return result;
    }


}
