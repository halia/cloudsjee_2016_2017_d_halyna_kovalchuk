package pl.kovalchuk;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class PasswordEntropyMapper extends Mapper<Object, Text, Text, DoubleWritable> {


    @Override
    protected void map(Object key, Text password, Context context) throws IOException, InterruptedException {
        double entropy = REntropy.getShannonEntropy(password.toString());
        context.write(password, new DoubleWritable(entropy));
    }
}
