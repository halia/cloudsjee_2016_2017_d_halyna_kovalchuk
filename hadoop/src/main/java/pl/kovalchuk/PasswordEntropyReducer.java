package pl.kovalchuk;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class PasswordEntropyReducer extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {

    @Override
    protected void reduce(Text password, Iterable<DoubleWritable> entropies, Context context) throws IOException, InterruptedException {
        for (DoubleWritable entropy : entropies) {
            context.write(password, entropy);
        }
    }
}
