package pl.kovalchuk;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.ToolRunner;

import java.io.File;

public class JobRunner {

    public void run(File inputFile, long id) throws Exception {
        Configuration configuration = new Configuration();
        ToolRunner.run(configuration,new JobTool(inputFile, id), new String[]{});
    }

}
