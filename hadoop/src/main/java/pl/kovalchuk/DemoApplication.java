package pl.kovalchuk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) throws Exception {
        System.setProperty("hadoop.home.dir", "C:\\");
		SpringApplication.run(DemoApplication.class, args);
		//new JobRunner().run();
	}
}
