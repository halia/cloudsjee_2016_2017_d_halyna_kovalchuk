package pl.kovalchuk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoginApplication {

	public static void main(String[] args) {
		System.setProperty("IGNITE_QUIET", "false");
		SpringApplication.run(LoginApplication.class, args);
	}
}
