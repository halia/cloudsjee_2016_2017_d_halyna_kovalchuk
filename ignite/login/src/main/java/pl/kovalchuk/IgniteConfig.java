package pl.kovalchuk;
/*закоментувані xml то по старому способі, можна видалити*/
import org.apache.ignite.cache.websession.WebSessionFilter;
import org.apache.ignite.startup.servlet.ServletContextListenerStartup;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;

@Configuration
public class IgniteConfig {
    
    @Bean
    public ServletContextListener igniteListener() {
        return new ServletContextListenerStartup();
    }
 
    @Bean
    public FilterRegistrationBean igniteFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setName("IgniteWebSessionsFilter");
        registration.setFilter(new WebSessionFilter());
        registration.addUrlPatterns("/*");
        registration.setOrder(Integer.MIN_VALUE);/*в цьому випадку працює spring security, змінює послідовінісь верифікування аутенфікації*/
        return registration;
    }

    @Bean
    public ServletContextInitializer igniteContextParam() {
        return new ServletContextInitializer() {
            @Override
            public void onStartup(ServletContext servletContext) throws ServletException {
                servletContext.setInitParameter("IgniteConfigurationFilePath", "ignite.xml");/*шлях до файлу з конфігурацією*/
                servletContext.setInitParameter("IgniteWebSessionsCacheName", "myCache");/*тут можна змінити на ту назву, але потрібно змінити в ignite.xml*/
            }
        };
    }
}
