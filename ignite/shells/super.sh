add-apt-repository ppa:openjdk-r/ppa
apt-get update
apt-get install -y openjdk-8-jdk dos2unix maven
cd /home/vagrant/app
mvn clean package
nohup java -jar -Dserver.port=8080 target/gs-securing-web-0.1.0.jar & sleep 1
nohup java -jar -Dserver.port=8081 target/gs-securing-web-0.1.0.jar & sleep 1